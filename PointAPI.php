<?php
class CurlException extends Exception {}
class PointAPIException extends Exception {}
class PointAPIResultException extends Exception {
  public $httpcode;
  public $errordata;
}

class PointAPI {
  private $username;
  private $apikey;
  private $curl;
  private $apiurl      = 'http://pointhq.com';
  private $curloptions = array(
    CURLOPT_FAILONERROR    => true,
    CURLOPT_HEADER         => false,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_SSL_VERIFYPEER => false,
    CURLOPT_SSL_VERIFYHOST => 0,
    CURLOPT_CONNECTTIMEOUT => 1,
    CURLOPT_ENCODING       => '',
    CURLOPT_USERAGENT      => 'sztanpet dns updater',
    CURLOPT_HTTPAUTH       => CURLAUTH_BASIC,
    CURLOPT_HTTPHEADER     => array(
      'Accept: application/json',
      'Content-type: application/json'
    ),
  );
  public $apicode;
  public $data;
  
  public function __construct( $username, $apikey ) {
    
    $this->$username = $username;
    $this->apikey    = $apikey;
    
  }
  
  protected function initCurl( Array $options = null ) {
    
    $options = $options? $options: $this->curloptions;
    $this->curl = curl_init();
    
    if ( !curl_setopt_array( $this->curl, $options ) )
      throw new CurlException("Unable to set an option");
    
    return $this;
    
  }
  
  protected function call( $method, $url, Array $data = null ) {
    
    $options = array(
      CURLOPT_URL     => $url,
      CURLOPT_USERPWD => $this->username . ':' . $this->apikey,
    ) + $this->curloptions;
    
    $method = strtolower( $method );
    switch( $method ) {
      
      default:
        throw new PointAPIException('Unknown method passed: ' . $method );
        break;
      
      case 'get':
        break;
      
      case 'post':
        $options[ CURLOPT_POST ] = true;
        break;
      
      case 'put':
        $options[ CURLOPT_CUSTOMREQUEST ] = 'PUT';
        break;
      
      case 'delete':
        $options[ CURLOPT_CUSTOMREQUEST ] = 'DELETE';
        break;
      
    }
    
    if ( $data )
      $options[ CURLOPT_POSTFIELDS ] = json_encode( $data );
    
    $this->initCurl( $options );
    $data = curl_exec( $this->curl );
    
    $this->apicode = curl_getinfo( $this->curl, CURLINFO_HTTP_CODE );
    
    if ( $data === false ) {
      
      if ( !$this->apicode ) {
        
        $errno = curl_errno( $this->curl );
        $error = curl_error( $this->curl );
        throw new CurlException('Curl error: ' . $errno . ':' . $error );
        
      }
      
    } else
      $this->data = json_decode( $data, true );
    
    curl_close( $this->curl );
    
    if ( $this->apicode > 400 ) {
      
      $exception = new PointAPIResultException('Request failed');
      $exception->httpcode  = $this->apicode;
      $exception->errordata = $this->data;
      
    }
    
    return $this->data;
    
  }
  
  public function __call( $name, $args ) {
    
    $name   = strtolower( $name );
    $method = 'handle';
    
    if ( strpos( $name, 'get' ) === 0 ) {
      
      $method    .= str_replace('get', '', $name );
      $httpmethod = 'get';
      
    }
    elseif ( strpos( $name, 'create' ) === 0 ) {
      
      $method    .= str_replace('create', '', $name );
      $httpmethod = 'post';
      
    }
    elseif ( strpos( $name, 'edit' ) === 0 ) {
      
      $method    .= str_replace('edit', '', $name );
      $httpmethod = 'put';
      
    }
    elseif ( strpos( $name, 'delete' ) === 0 ) {
      
      $method    .= str_replace('delete', '', $name );
      $httpmethod = 'delete';
      
    } else
      $method = $name;
    
    if ( !method_exists( $this, $method ) )
      throw new PointAPIException( 'the method ' . $name . ' was not found' );
    
    array_unshift( $args, $httpmethod );
    return call_user_func_array( array( $this, $method ), $args );
    
  }
  
  protected function getURL( $method, $baseurl, $id = null ) {
    
    $url = $baseurl;
    
    switch( $method ) {
      
      case 'post':
        break;
      
      case 'get':
        
        if ( $id !== null )
          $url .= '/' . $id;
        
        break;
      
      case 'put':
      case 'delete':
        
        if ( $id === null )
          throw new PointAPIException('no id passed for ' . $method );
        
        $url .= '/' . $id;
        
        break;
      
      default:
        throw new PointAPIException('unknown method: ' . $method );
        break;
      
    }
    
    return $url;
    
  }
  
  protected function handleZone( $method, $zoneid = null, Array $data = null ) {
    
    $url = $this->getURL(
      $method,
      $this->apiurl . '/zones',
      $zoneid
    );
    
    return $this->call( $method, $url, $data );
    
  }
  
  protected function handleRecord( $method, $zoneid, $recordid = null, Array $data = null ) {
    
    $url = $this->getURL(
      $method,
      $this->apiurl . '/zones/' . $zoneid . '/records',
      $recordid
    );
    
    return $this->call( $method, $url, $data );
    
  }
  
}
