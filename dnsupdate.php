<?php
error_reporting( E_ALL );
define('BASE_PATH', dirname( __FILE__ ) . '/' );
define('CACHEFILE', '/var/tmp/myip');
define('CONFIG',    BASE_PATH . 'config.php');
require CONFIG;

function didMyIpChange( $newip ) {
  
  if ( is_file( CACHEFILE ) ) {
    
    $oldip = file_get_contents( CACHEFILE );
    if ( $oldip == $newip )
      return false;
    
  }
  
  return true;
  
}

$myip = file_get_contents('http://ec2.sztanpet.net/myip.php');

if ( !$myip )
  throw new Exception('Unable to get ipaddress');

$myip = json_decode( $myip, true );
$GLOBALS['myip'] = $myip = $myip['ip'];

if ( !$myip )
  throw new Exception('Unable to get ipaddress');

if ( didMyIPChange( $myip ) )
  exit(0);

$dir = new GlobIterator(
  ONUPDATEDIR . '/*.php',
  FilesystemIterator::CURRENT_AS_PATHNAME |
  FilesystemIterator::SKIP_DOTS | FilesystemIterator::FOLLOW_SYMLINKS
);

foreach( $dir as $path ) {
  
  if ( !is_readable( $path ) ) {
    
    echo $path, " is unreadable, continuing\n";
    continue;
    
  }
  
  include $path;
  
}

file_put_contents( CACHEFILE, $myip );
