<?php
if ( !defined('BASE_PATH') ) {
  define('BASE_PATH', realpath( dirname(__FILE__) . '/..') . '/');
  define('CONFIG',    BASE_PATH . 'config.php');
  require_once CONFIG;
}

require_once BASE_PATH . "PointAPI.php";

$myip = $GLOBALS['myip'];
$api  = new PointAPI( POINTAPI_USERNAME, POINTAPI_KEY );
$data = $api->getZone();

$zone = current( $data );
$zone = $zone['zone'];

$data = $api->getRecord( $zone['id'] );

$updatewith = array(
  'zone_record' => array(
    'data' => $myip,
  ),
);

foreach( $data as $record ) {
  
  $record = $record['zone_record'];
  
  if ( !in_array( $record['name'], $GLOBALS['hostnamesToUpdate'] ) )
    continue;
  
  if ( $record['data'] == $myip )
    continue;
  
  if ( $record['record_type'] != 'A' )
    continue;
  
  $ret = $api->editRecord(
    $zone['id'],
    $record['id'],
    $updatewith
  );
  
  echo "  - ", $record['name'], ' successfully updated from: ', $record['data'], ' to: ', $myip, "\n";
  
}
