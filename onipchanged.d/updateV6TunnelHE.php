<?php
if ( !defined('BASE_PATH') ) {
  define('BASE_PATH', realpath( dirname(__FILE__) . '/..') . '/');
  define('CONFIG',    BASE_PATH . 'config.php');
  require_once CONFIG;
}

echo "Updating v6 tunnel: ";
exec(
  'curl -sS "https://ipv4.tunnelbroker.net/nic/update?' .
    'username='  . V6TUNNEL_USERNAME .
    '&password=' . V6TUNNEL_KEY .
    '&hostname=' . V6TUNNEL_TUNNELID . '"',
  $output,
  $exitcode
);
echo "curl exited with exitcode: ", $exitcode, " response was: ", implode("\n", $output ), "\n";
